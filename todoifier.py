#!/usr/bin/env python3
try:
	from termcolor import colored
except:
	colored = lambda x,y: x

from platform import system
from time import sleep
import json
import os
import re
import sys
import argparse
from math import log10

# Yay Windows and their crappy terminals!
if system() == "Windows":
	from colorama import init
	from colorama import deinit
	from atexit import register
	init()
	register(deinit)

lang = {}
root = "."   # Default to scanning the current directory
recurse = -1 # Number of directories to scan down. -1 is all, 0 is target directory only
loop = 0     # Second in between each iteration. 0 will run only once
noclear = False
nocolor = False
outfile = None
showpath = False

write_output = lambda ls: [print(x) for x in ls]
to_file = lambda ls: outfile.writelines([x + "\n" for x in ls])

# TODO: Move to external file
tags = {
	"TODO":(5,"yellow"),
	"FIXME":(10,"red"),
	"OPTIMIZE":(2,"green"),
	"CLEAN":(1,"blue"),
	"CRITICAL":(9999,"magenta")
}


def scan_file(filename):
	with open(filename,"r") as file:
		data = file.readlines()

	name,ext = os.path.splitext(filename)
	output = []
	for i in range(len(data)):
		m = lang[ext].search(data[i])
		if m != None:
			output.append((m.group(1),m.group(2),i+1))
	return output
	

def regexify(ls):
	regex  = "(?:" + "|".join([l["start"].replace("*","\*") for l in ls]) + ") *"   # Find the start of a comment
	regex += "(" + "|".join([t for t in tags]) + "):* *"                            # Match the tag type
	regex += "(.*?)"                                                                # Match the tag text
	regex += " *(?:"
	temp   = "|".join([l["end"].replace("*","\*") for l in ls if "end" in l.keys()])
	regex += temp if temp != "|" else ""
	regex += ")?$"
	return re.compile(regex)

# TODO: Load tag settings from file here
def load_settings():
	
	def load_lang(path):
		global lang

		for f in os.listdir(path):
			with open(path + "/" + f,"r") as file:
				spec = json.loads(file.read())
			lang[spec["filext"]] = regexify(spec["style"])

	# TODO: Consider loading these paths from a config file?
	# Note that the latter paths WILL override the upper paths if there duplicate filetypes
	[load_lang(path) for path in ["/usr/share/todoifier/lang", os.environ["HOME"] + "/.todoifier/lang", "./lang"] if os.path.exists(path)]

	if lang == {}:
		print("ERROR: Language files could not be found in either /usr/share/todoifier/lang, ~/.todifier/lang, or ./lang")
		return False

	return True

def print_output(output, filename=" "):
	global write_output
	if filename != " ":
		filename += ":"
	sort = sorted(output, reverse=True, key=lambda s: tags[s[0]][0]) # Sort by priority
	sigdigs = int(log10(max(sort, key=lambda s: s[2])[2])) + 1		 # Extract the number of digits in the largest line number
	maxtag = len(max(sort, key=lambda s: len(s[0]))[0])     		 # Get the largest tag string's length 
	output = []

	for s in sort:
		output.append(filename + "{1: >{0}}:".format(sigdigs,s[2]) + colored(" {1: ^{0}} ".format(maxtag,s[0]),tags[s[0]][1]) + s[1])
	write_output(output)


# TODO: Handle more arguments
def handle_args():
	global root
	global recurse
	global loop
	global noclear
	global nocolor
	global outfile
	global showpath

	global write_output

	parser = argparse.ArgumentParser()

	parser.add_argument("-d","--directory",help="Path to the source directory to scan (default is ./)",type=str)
	parser.add_argument("-r","--recurse",help="Number of levels to recurse. Set to zero for only the target directory. Set to -1 to traverse all subdirs. (default is -1)",type=int)
	parser.add_argument("-l","--loop",help="Time interval in seconds for the script to clear and run again. Set to -1 for a single execution. (default is 0)",type=int)
	parser.add_argument("-n","--noclear",help="Do not call 'clear' after each loop. Ignored if -l is not set or set to 0",action="store_true")
	parser.add_argument("-c","--nocolor",help="Disable text coloring versus ANSI escape sequences",action="store_true")
	parser.add_argument("-o","--output",help="Send output to the following file. If used with -l, truncates the file each time. Implies -c, -n",type=str)
	parser.add_argument("-p","--showpath",help="Print the whole path for each file",action="store_true")

	args = parser.parse_args()

	root = args.directory or root
	recurse = args.recurse or recurse
	loop = args.loop or loop
	noclear = args.noclear or noclear
	nocolor = args.nocolor or nocolor
	if args.output:
		outfile = open(args.output, "w")
		nocolor = True
		noclear = True
		write_output = to_file
	showpath = args.showpath or showpath

def main():
	# TODO: Implement levels of recursion
	# TODO: Implement directory exclusion
	# TODO: Implement a different recursion method
	for (dirpath,dirnames,filenames) in os.walk(root):
		for f in filenames:
			output = [] 
			name,ext = os.path.splitext(f)
			if ext in lang.keys():
				output = scan_file(os.path.join(dirpath, f))
			# TODO: Allow sorting type (by file, by priority)
			if output != []:
				# TODO: Clean up the crappy output function calls
				if (showpath):
					write_output([os.path.join(dirpath, f) + ":"])
				else:
					write_output([f + ":"])
				print_output(output)
				write_output([""])

if __name__ == "__main__":
	if len(sys.argv) != 1:
		handle_args()
	if not load_settings():
		exit(1)
	if nocolor:
		colored = lambda x,y: x

	if (loop == 0):
		main()
		exit()

	# FIXME: Fix file descripter leak
	while True:
		if not noclear:
			os.system("clear")
		if outfile:
			outfile.truncate(0)
		main()
		if outfile:
			outfile.flush()
		sleep(loop)
