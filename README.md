Todoifier
=========

If you code anything like how I do, there are more TODO: statements than there are lines of code.
Todoifier provides an easy way to output all of these messages to the terminal, or that could be embedded into another system.

Dependencies
------------
 - Python 3
 - Termcolor module
 - Colorama module (for windows users)

Features
-----

This is very much in development, so the arguments are subject to change.
Currently planned are the following features:

 - Custom TODO tags, with varying levels of importance. (e.g. FIXME, CLEAN...)
 - Customizable colors
 - Specify comment patterns (will look for C-like comments by default)

